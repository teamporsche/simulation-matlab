function [ newValues ] = CarModel( currentValues,delta,duty,timeStep)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
global deltaVar;
global D;
D = duty;
deltaVar = delta;

y = ode4(@car,[0 0+timeStep],currentValues);
newValues = y(2,:);

end

