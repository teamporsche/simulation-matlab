function [ delta ] = SteeringController( xRef,yRef,index)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    global car;
    
    Kp = 0.5;
    Ki = 0.00001;
    
    x = car(index).x;
    y = car(index).y;
    psi = car(index).psi;
 
    refAngle = atan2(yRef-y, xRef-x);
    refAngle = mod(refAngle,2*pi);
 
%     error = refAngle-psi;
    error = refAngle-mod(psi,2*pi);
    %Keep error between -pi and pi
    if(error < -pi)
        error = error + 2*pi;
    end
    if(error > pi)
        error = error - 2*pi;
    end
 
    car(index).steeringIntegration = car(index).steeringIntegration + error;
 
    delta = error*Kp + car(index).steeringIntegration*Ki;
 
    %Truncating the steering angle since it has a limit
    if(delta < -pi/6)
        delta = -pi/6;
    end
    if(delta > pi/6)
        delta = pi/6;
    end
 
end