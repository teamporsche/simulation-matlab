function [ dist ] = DistCars(car1, car2)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    global Track;
    if car1.I < car2.I
        dist = sum(sqrt((Track(1,car1.I:car2.I-1)-Track(1,car1.I+1:car2.I)).^2 ...
                   +(Track(2,car1.I:car2.I-1)-Track(2,car1.I+1:car2.I)).^2));
    else
        dist = sum(sqrt((Track(1,1:end-1)-Track(1,1+1:end)).^2+ ...
                  (Track(2,1:end-1)-Track(2,1+1:end)).^2))- ...
               sum(sqrt((Track(1,car2.I:car1.I-1)-Track(1,car2.I+1:car1.I)).^2+ ...
                   (Track(2,car2.I:car1.I-1)-Track(2,car2.I+1:car1.I)).^2));%-;
    end
end