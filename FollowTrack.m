function [ xRef, yRef ] = FollowTrack( carIndex )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    global car;
    global Track;
    
    x = car(carIndex).x;
    y = car(carIndex).y;
    psi = car(carIndex).psi;
    Position = [x y];
    Direction = [cos(psi) sin(psi)];
    refPoint = car(carIndex).refPoint;
    lastIndex = car(carIndex).lastIndex;
    xRef = -1;
    yRef = -1;
    
    
    
quality = 9999;
index=-1;
for i=1:length(Track) %For each point
    point = [Track(1,i) Track(2,i)];
    
    %Distance to point
    lengthToPoint = norm(point-Position);
        
    %Angle
    TempPoint=point-Position;
    angleToPoint = acos((Direction*TempPoint')/(norm(Direction)*norm(TempPoint)));
    
    %Distance to last point
    if isnan(refPoint)
        lengthToLast = 0;
    else
        lengthToLast = norm(point-refPoint);
    end
    
    %Last index
    if isnan(lastIndex)
        indexDistance = 0;
    else
        temp1 = abs(lastIndex-i);
        temp2 = abs(lastIndex-length(Track)-i);
        if temp1 < temp2
            indexDistance = temp1;
        else
            indexDistance = temp2;
        end
    end
    
    %Quality
    tempQuality = lengthToPoint + 2*angleToPoint + lengthToLast + 0.1*indexDistance;
    if tempQuality < quality && lengthToPoint > 0.1
        quality = tempQuality;
        index = i;
    end
    

end

if(index == -1) %Checks if a point was found
    car(carIndex).refSpeed = 0;
    car(carIndex).refAngle = 0;
    disp('No reference point found!')
    refPoint = NaN;
    lastIndex = NaN;
else
    refPoint = [Track(1,index) Track(2,index)];
    lastIndex = index;
    %disp('quality');
    %disp(quality);
    
    %Speed
    car(carIndex).refSpeed = Track(3,index);
    
    %Stearing
    refVektor = [Track(1,index) Track(2,index)]-Position;
    car(carIndex).refAngle = atan(refVektor(2)/refVektor(1)) + gt(0,refVektor(1))*pi;
    %Keeping the refAngle between -pi and pi
    if(car(carIndex).refAngle < -pi)
        car(i).refAngle = car(i).refAngle + 2*pi;
    end
    if(car(carIndex).refAngle > pi)
        car(carIndex).refAngle = car(carIndex).refAngle -2 *pi;
    end
end
xRef = refPoint(1);
yRef = refPoint(2);

end

