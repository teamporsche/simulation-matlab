clear all,close all, clc, format compact
 
global car;
global Track;

global timeStep;
 
Track = [1.5  1   0  -1 -1.5 -1   0   1 1.5;
              0  -1 -1.5 -1   0   1  1.5  1 0;
             0.5 0.5 0.7 0.7 0.5 0.5 0.7 0.7 0.5];
v=0:0.0001:2*pi;
temp = linspace(0,60,length(v));
% Track =[temp; zeros(1,length(v)); v*0+0.7];
%Track = [2*cos(v)+0.1*cos(8*v); 2*sin(v)+0.1*sin(8*v); [0.4*ones(1,length(v)/2),0.8*ones(1,length(v)/2)]];
Track =  [2*cos(v); 2*sin(v); [0.4*ones(1,length(v)/2),0.8*ones(1,length(v)/2)]];
runTime = 60;
timeStep = 0.01;
nSteps = runTime/timeStep;
 
nCars = 4;
for i = 1:nCars
   % car(i).x = 0.2*(rand(1)-1/2);
    car(i).x = 2*cos(pi/nCars*(nCars-1-i));
%   car(i).y = -1.5+0.2*(rand(1)-1/2);
    car(i).y = 2*sin(pi/nCars*(nCars-1-i));
    car(i).v = 0;
    car(i).psi = pi/nCars*(nCars-1-i)+pi/2;
    car(i).I = 0;
    car(i).values = zeros(nSteps,8);
    car(i).steeringIntegration = 0;
    car(i).distanceIntegrationPre = 0;
    car(i).distanceIntegrationLeader = 0;
    car(i).distancePrevErrorLeader = 0;
    car(i).distancePrevErrorPre = 0;
    car(i).refPoint = 0;
    car(i).lastIndex = 0;
    car(i).refSpeed = 0;
    car(i).refAngle = 0;
    car(i).desiredDistance = 1;
    car(i).duty = 0;
    car(i).delta = 0;
end
 
T = 0:timeStep:(timeStep*nSteps);
T = T';
deltaArray = zeros(nSteps,1);
 
duty = 1;

distanceCars = zeros(nSteps,nCars-1);
 
for i=1:nSteps+1
    for j=1:nCars
        
        car(1).v = 0.5+0.2*(i-nSteps/2)/nSteps*(i > nSteps/2)*(i < nSteps*7/8);
        newValues = [car(j).x car(j).y car(j).psi car(j).v];
        
        [xRef, yRef, I] = FollowTrack2(car(j));
        car(j).I = I;
        car(i).delta = SteeringController(xRef,yRef,j);
        deltaArray(i+1) = car(i).delta;
        if j > 1
            PlatooningController(j);
        else
            
            car(1).duty = 0;
        end

         
        newValues = CarModel(newValues,car(i).delta,car(j).duty,timeStep);
        car(j).values(i,:) = [newValues, xRef, yRef, I, car(i).delta];
        car(j).x = newValues(1); car(j).y = newValues(2); 
        car(j).psi = newValues(3); car(j).v = newValues(4);
%         car(j).duty
        
        
        
        if j > 1
            DistCars(car(j), car(j-1));
            distanceCars(i,j-1) = DistCars(car(j), car(j-1));
        end
    end
end
 
xyPlot = figure;
hold on, grid on, title('X-Y plot'), xlabel('X'), ylabel('Y') 
mi = min([Track(1,:), Track(2,:)]);
ma = max([Track(1,:), Track(2,:)]);
plot(Track(1,:),Track(2,:), 'c')
 
color = 'rgbmyk';
for i=1:100:size(car(2).values,1)
    plot(Track(1,1:100:end),Track(2,1:100:end), 'c'); 
    hold on
    for j= 1:nCars
        h = plot(car(j).values(1:i,1),car(j).values(1:i,2),[color(j)]);
        h = plot(car(j).values(i,1),car(j).values(i,2),[color(j) 'o']);
    end
    axis(1.5*[mi-1, ma+1, mi-1, ma+1])
    pause(0.0001);
    hold off;
end
 
% figure(xyPlot);
% for i=1:size(car(j).values,1)
%     for j=1:nCars
%         h(j) = plot(car(j).values(i,1),car(j).values(i,2),'r');
%     end
% %     pause(0.0001);
% %     delete(h);
% end
 
% color = ['r' 'k' 'b' 'g'];
figure
for j=1:nCars
    %figure
    %plot(T,deltaArray*180/pi), grid on
    %title('Steering angle delta'),xlabel('Time'),ylabel('Degrees')
    
    subplot(nCars,1,j)
%     plot(T,car(j).values(:,1),T,car(j).values(:,2),T,car(j).values(:,3),'r',T,car(j).values(:,4),'k')
%     legend('X','Y','psi','V'), xlabel('Time [s]')
    plot(T,car(j).values(:,1),T,car(j).values(:,2),T,car(j).values(:,5),'r',T,car(j).values(:,6),'k')
    legend('X','Y','refX','refY'), xlabel('Time [s]')
%     axis([0 runTime])
    grid on
    
    %figure(xyPlot);
    %plot(car(j).values(:,1),car(j).values(:,2),color(j));
    axis([0 runTime min([min(car(j).values(:,5)) min(car(j).values(:,6))])...
          max([max(car(j).values(:,5)) max(car(j).values(:,6))])])
end


figure 
for j = 1:(nCars-1)
    hold all
    plot(T, 1-distanceCars(:,j))
    title('distance error')
end
legend('car 1-2','car 2-3','car 3-4')