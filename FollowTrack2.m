function [ xRef, yRef, I] = FollowTrack2(car)
    global Track;
    [V,I]= min((car.x-Track(1,:)).^2+(car.y-Track(2,:)).^2);
%     
%     if(I<car.I)
%         I=car.I+1;
%     end
    
    I=mod(I+5,length(Track));
    if I == 0
        I=1;
    end
    P = Track(1:2,I);
    xRef = P(1); yRef=P(2);
end