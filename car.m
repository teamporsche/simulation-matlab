function dy = car(t,y)

global deltaVar;
global D;

lf = 32.5*10^(-3);
lr = 32.5*10^(-3);
l = lf + lr;
C1 = lr/l;
C2 = 1/l;
Cm1 = 11.52;
Cm2 =  2.74;
Cr2 = 0.05;
Cr0 = 0.54;

dy = zeros(4,1);
dy(1) = y(4) * cos(y(3) + C1*deltaVar);
dy(2) = y(4) * sin(y(3) + C1*deltaVar);
dy(3) = y(4)*deltaVar*C2;

saturated_v = min(1, max(-1, 10*y(4)));
% dy(4) = Cm1 * D - Cm2 *D *v -Cr2 * v^2 -Cr0*saturated_v -(v*delta)^2 * C2;

dy(4) = Cm1 * D - Cm2 *D *y(4) -Cr2 * y(4)^2 -Cr0*saturated_v -(y(4)*deltaVar)^2 * C2;