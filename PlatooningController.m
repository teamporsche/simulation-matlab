function PlatooningController( index )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%     global timeStep;
%     s = tf('s');
%     sys = (4717*s-3.869)/(s^2+428.8*s+1258);
%     [A,B,C,D] = tf2ss(sys.num{1},sys.den{1});
%     sys = ss(A,B,C,D,timeStep);

    global car  timeStep;
    Kp = 0.4; %/(1-car(index).delta/0.6982);
    Ki = 0.1;
    Kd = 0.3;
    eta = 0.5;
    
 
    %% Pre
    error_pre = (DistCars(car(index),car(index-1)) - car(index).desiredDistance);
    car(index).distanceIntegrationPre = car(index).distanceIntegrationPre +...
                                     error_pre * timeStep;
    derivative = (error_pre - car(index).distancePrevErrorPre)/timeStep;
    car(index).duty = Kp * error_pre + Ki * car(index).distanceIntegrationPre + Kd * derivative;
    car(index).distancePrevErrorPre = error_pre;
    
    
    %% Leader
    error_leader = DistCars(car(index),car(1)) - car(index).desiredDistance * (index-1);   
    car(index).distanceIntegrationLeader = car(index).distanceIntegrationLeader +...
                                     error_leader * timeStep;
    derivative = (error_leader - car(index).distancePrevErrorLeader)/timeStep;
    car(index).duty = eta*car(index).duty + (1-eta)*(Kp * error_leader + Ki * car(index).distanceIntegrationLeader + Kd * derivative);
    car(index).distancePrevErrorLeader = error_leader;
    
%     error = (error_pre * eta + error_leader*(1-eta));
    
    if abs(car(index).duty) > 1 
        car(index).duty = sign(car(index).duty);
    else
        car(index).duty = car(index).duty;
    end
%     x = car(index).x;
%     y = car(index).y;
%     psi = car(index).psi;
%     v = 1;
%     acceleration = 3.848*10^-7*prevAcceleration ...
%                    - 0.003071*prevDuty - ...
%                    4.292*10^-6 * prevPrevDuty;
%     car(index).v = acceleration * timeStep + car(index).v;
     
    
    
end

