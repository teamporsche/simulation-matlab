clear all,close all, clc, format compact

global car;
global Track;

Track = [1.5  1   0  -1 -1.5 -1   0   1;
              0  -1 -1.5 -1   0   1  1.5  1;
             0.5 0.5 0.7 0.7 0.5 0.5 0.7 0.7];
runTime = 5;
timeStep = 0.01;
nSteps = runTime/timeStep;

nCars = 4;
for i = 1:nCars
    car(i).x = (i-1)*0.5;
    car(i).y = 0;
    car(i).v = 0;
    car(i).psi = 0;
    car(i).values = zeros(nSteps+1,4);
    car(i).steeringIntegration = 0;
    car(i).refPoint = 0;
    car(i).lastIndex = 0;
    car(i).refSpeed = 0;
    car(i).refAngle = 0;
end

T = 0:timeStep:timeStep*nSteps;
T = T';
deltaArray = zeros(nSteps+1,1);

duty = 1;

for i=1:nSteps
    for j=1:nCars
        
        newValues = [car(j).x car(j).y car(j).psi car(j).v];
        [xRef, yRef] = FollowTrack(j);
        delta = SteeringController(xRef,yRef,j);
        deltaArray(i+1) = delta;
        vRef = PlatooningController(j);

        newValues = CarModel(newValues,delta,duty,timeStep);
        car(j).values(i+1,:) = newValues;
        car(j).x = newValues(1); car(j).y = newValues(2); 
        car(j).psi = newValues(3); car(j).v = newValues(4);
    end
end

xyPlot = figure;
hold on, grid on, title('X-Y plot'), xlabel('X'), ylabel('Y') 
mi = min([Track(1,:), Track(2,:)]);
ma = max([Track(1,:), Track(2,:)]);
axis([mi-1, ma+1, mi-1, ma+1])
plot(Track(1,:),Track(2,:), 'c')

figure(xyPlot);
for i=1:size(car(j).values,1)
    for j=1:nCars
        h(j) = plot(car(j).values(i,1),car(j).values(i,2),'r');
    end
    pause(0.0001);
    delete(h);
end

color = ['r' 'k' 'b' 'g'];
for j=1:nCars
    figure
    plot(T,deltaArray*180/pi), grid on
    title('Steering angle delta'),xlabel('Time'),ylabel('Degrees')
    
    figure
    plot(T,car(j).values(:,1),T,car(j).values(:,2),T,car(j).values(:,3),'r',T,car(j).values(:,4),'k')
    legend('X','Y','psi','V'), xlabel('Time [s]')
    grid on
    
    figure(xyPlot);
    plot(car(j).values(:,1),car(j).values(:,2),color(j));
end
axis equal



