lf = 32.5*10^(-3);
lr = 32.5*10^(-3);
l = lf + lr;
%     grad = 10;
%     rad = grad*pi/180;
m = 0.1;
a = 80*10^(-3);
b = 35*10^(-3);
J = 1/12*m*(a^2+b^2);
Cf = 0.01;
Cr = 0.02;

% constants
C1 = lr/l;
C2 = 1/l;
% Cm1 = k_M * N_Tr * V_Bat * r_w/(R*(r_w^2*m + J));
% Cm2 = (k_M*N_Tr)^2/(R*(r_w^2*m + J));
Cm1 = 11.52;
Cm2 =  2.74;
Cr2 = 0.05;
Cr0 = 0.54;

c = zeros(6,1);
c(1) = C1;
c(2) = C2;
c(3) = Cm1;
c(4) = Cm2;
c(5) = Cr2;
c(6) = Cr0;
